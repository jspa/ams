package org.example.common.cache;

import lombok.Data;

import java.time.LocalDateTime;

/**
 * 逻辑过期方案 - redis缓存对象
 * @author cheval
 */
@Data
public class RedisData {
    private static final Long serialVersionUID = 1L;

    /** 逻辑过期时间 */
    private LocalDateTime expireTime;

    /** 缓存对象 */
    private Object obj;
}
