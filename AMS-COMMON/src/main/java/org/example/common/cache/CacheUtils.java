package org.example.common.cache;

import cn.hutool.json.JSONUtil;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.script.RedisScript;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.List;
import java.util.UUID;
import java.util.concurrent.TimeUnit;
import java.util.function.Function;
import java.util.function.Supplier;

/**
 * cache utils
 *
 * @param <P> query param type
 * @param <R> data type of return
 * @author cheval
 */
public class CacheUtils<P, R> {
    private StringRedisTemplate redisTemplate;
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    private final Integer LOCK_TIMEOUT = 10;
    private final String LOCK_NAME_PRE = "lock:";
    private Integer CACHE_EXPIRED_TIME = 30;
    private TimeUnit CACHE_EXPIRED_TIME_UNIT = TimeUnit.MINUTES;
    private final Integer RE_TRY_QUERY_TIME = 1000;
    private final Integer CACHE_LOGICAL_EXPIRATION_TIME = 30;



    public CacheUtils(StringRedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }


    public CacheUtils(StringRedisTemplate redisTemplate,Integer cacheExpireTime,TimeUnit timeUnit) {
        this.redisTemplate = redisTemplate;
        if (cacheExpireTime != null && cacheExpireTime.intValue() != 0) {
            this.CACHE_EXPIRED_TIME = cacheExpireTime;
        }
        if (timeUnit != null) {
            this.CACHE_EXPIRED_TIME_UNIT = timeUnit;
        }
    }

    public CacheUtils(StringRedisTemplate redisTemplate, ThreadPoolTaskExecutor threadPoolTaskExecutor) {
        this.redisTemplate = redisTemplate;
        this.threadPoolTaskExecutor = threadPoolTaskExecutor;
    }

    /**
     * get current thread signature.
     * @return
     */
    public String getCurrentThreadSign() {
        return UUID.randomUUID().toString().replace("_","") + "_" + Thread.currentThread().getId();
    }

    /**
     * Get mutex lock. If it fails,return false,otherwise,return true.
     * 缺点： 不可重入（A 方法调用 B方法，A获取到了m锁，而B也要获取m锁，但是由于不可重入，B需要等A释放锁，最终导致死锁）
     *       不可重试： 锁一旦获取失败，就不会再次重试
     *       超时释放： 超时释放锁导致误删锁问题仍然可能发生
     *       主从不一致：主节点负责写，从节点负责读，主从节点同步需要一定时间，当我们获取锁的时候，需要向主节点写入锁，假设现在从节点
     *                 还没有同步锁，此时有又有请求尝试获取锁，他要读取从节点判断锁是否已经存在，但是由于没有及时同步，所以就出现
     *                 了多个线程获取到了同一把锁的问题。
     *
     * 解决缓存击穿问题对多个线程获取同一把锁影响不是很大，只要保证不是很多请求获取到同一把锁就行，所以可以采用自己实现的分布式锁，但是
     * 对于要求数据一致的业务就需要redission 实现的分布式锁了，比如银行转账业务。
     * @param lockName lock name
     * @return
     */
    private boolean lock(String lockName,String threadUniqueSign) {
        // lockName formation: lock:business type:identifier
        return redisTemplate.opsForValue().setIfAbsent(lockName, threadUniqueSign, LOCK_TIMEOUT, TimeUnit.SECONDS);
    }


    /**
     * Release lock.
     *
     * @param lockName
     * @return
     */
    private boolean unlock(String lockName,String threadUniqueSign) {
        // 采用redis执行lua脚本的原子性，避免锁误删
        String script = "local lockName = KEYS[1]\n" +
                "local currentThreadSign = ARGV[1]\n" +
                "\n" +
                "-- 获取key对应的标识\n" +
                "local localThreadSign = redis.call('get',lockName)\n" +
                "\n" +
                "-- 判断该锁是否是当前线程的，如果是就释放，不是就返回0\n" +
                "if(currentThreadSign == localThreadSign) then\n" +
                "    return redis.call('del',lockName)\n" +
                "end\n" +
                "\n" +
                "return 0";
        return redisTemplate.execute(RedisScript.of(script,Boolean.class), Collections.singletonList(lockName),threadUniqueSign);
    }


    /**
     * Mutex lock solves cache breakdown problem.(For query database operation to add mutex lock.)
     * Cache Breakdown： Hotspot key suddenly expires. A large number of requests hit the database directly,causing database downtime.
     *
     * @param queryDB      query function
     * @param param        param of query
     * @param businessType business type，to generate mutex lock name.
     * @param identifier   business unique identifier，to generate mutex lock name.
     * @param resClass     class type of return object.
     * @param cacheKey     cache key
     * @return <R>
     */
    public List<R> mutexQuery(Function<P, List<R>> queryDB, P param, String businessType, String identifier, Class<R> resClass, String cacheKey) {
        // 1. first query cache.
        String jsonStr = redisTemplate.opsForValue().get(cacheKey);
        if (jsonStr != null) {
            // 1.1 if existed,return result directly.
            return JSONUtil.toList(jsonStr,resClass);
        }
        // 1.2 if not exist,mutex query database.
        // 1.2.1 get mutex lock.
        String lockName = LOCK_NAME_PRE + businessType + ":" + identifier;
        // 1.2.2 get thread unique sign.
        String currentThreadSign = getCurrentThreadSign();
        boolean isLock = lock(lockName,currentThreadSign);
        if (isLock) {
            // after got lock, query database and cache data to redis.
            List<R> data = queryDB.apply(param);
            // if the data of result is null,set the cache time shorter to prevent cache penetration.
            redisTemplate.opsForValue().set(cacheKey, JSONUtil.toJsonStr(data), CACHE_EXPIRED_TIME, CACHE_EXPIRED_TIME_UNIT);
            // release lock
            unlock(lockName,currentThreadSign);
            return data;
        } else {
            // not got lock,wait a little time,then try get data again.
            try {
                Thread.sleep(RE_TRY_QUERY_TIME);
                return mutexQuery(queryDB, param, businessType, identifier, resClass, cacheKey);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
    }

    /**
     * Mutex lock solves cache breakdown problem.(For query database operation to add mutex lock.)
     * Cache Breakdown： Hotspot key suddenly expires. A large number of requests hit the database directly,causing database downtime.
     *
     * @param queryDB      query function
     * @param businessType business type，to generate mutex lock name.
     * @param identifier   business unique identifier，to generate mutex lock name.
     * @param resClass     class type of return object.
     * @param cacheKey     cache key
     * @return <R>
     */
    public List<R> mutexQuery(Supplier<List<R>> queryDB, String businessType, String identifier, Class<R> resClass, String cacheKey) {
        // 1. first query cache.
        String jsonStr = redisTemplate.opsForValue().get(cacheKey);
        if (jsonStr != null) {
            // 1.1 if existed,return result directly.
            return JSONUtil.toList(jsonStr,resClass);
        }
        // 1.2 if not exist,mutex query database.
        // 1.2.1 get mutex lock.
        String lockName = LOCK_NAME_PRE + businessType + ":" + identifier;
        // 1.2.2 get thread unique sign.
        String currentThreadSign = getCurrentThreadSign();
        boolean isLock = lock(lockName,currentThreadSign);
        if (isLock) {
            // after got lock, query database and cache data to redis.
            List<R> data = queryDB.get();
            // if the data of result is null,set the cache time shorter to prevent cache penetration.
            redisTemplate.opsForValue().set(cacheKey, JSONUtil.toJsonStr(data), CACHE_EXPIRED_TIME, CACHE_EXPIRED_TIME_UNIT);
            // release lock
            unlock(lockName,currentThreadSign);
            return data;
        } else {
            // not got lock,wait a little time,then try get data again.
            try {
                Thread.sleep(RE_TRY_QUERY_TIME);
                return mutexQuery(queryDB, businessType, identifier, resClass, cacheKey);
            } catch (InterruptedException e) {
                e.printStackTrace();
                return null;
            }
        }
    }


    /**
     * Logical expiration solves cache breakdown.(For query database operation to use logical expiration solution.)
     * @param queryDB query function
     * @param param   param of query
     * @param businessType  query business type
     * @param identifier    business type unique identifier
     * @param resClass      class type of return
     * @param cacheKey      data cache key
     * @return <R>
     */
    public R logicalExpirationQuery(Function<P, R> queryDB, P param, String businessType, String identifier, Class<R> resClass, String cacheKey) {
        // query cache
        String jsonStr = redisTemplate.opsForValue().get(cacheKey);
        RedisData data = JSONUtil.toBean(jsonStr, RedisData.class);
        if (jsonStr != null && data.getExpireTime().isAfter(LocalDateTime.now())) {
            // data is not null and no expired,return data directly.
            return JSONUtil.toBean(JSONUtil.toJsonStr(data.getObj()), resClass);
        } else {
            // data is null or data had expired,so start new thread to request data and cache data to redis,then return null or return old data.
            // try get lock
            String lockName = LOCK_NAME_PRE + businessType + ":" + identifier;
            // 1.2.2 get thread unique sign.
            String currentThreadSign = getCurrentThreadSign();
            boolean isLock = lock(lockName,currentThreadSign);
            if (isLock) {
                // start new thread to request data
                Runnable task = () -> {
                    R result = queryDB.apply(param);
                    // encapsulation redis data
                    RedisData redisData = new RedisData();
                    redisData.setObj(result);
                    redisData.setExpireTime(LocalDateTime.now().plusMinutes(CACHE_LOGICAL_EXPIRATION_TIME));
                    // cache redis data to redis
                    redisTemplate.opsForValue().set(cacheKey,JSONUtil.toJsonStr(redisData));
                    // release lock
                    unlock(lockName,currentThreadSign);
                };
                threadPoolTaskExecutor.execute(task);
            }
            // return null or old data directly
            return jsonStr == null ? null : JSONUtil.toBean(JSONUtil.toJsonStr(data.getObj()), resClass);
        }
    }


    /**
     * For query add cache.
     *
     * @param queryDB      query function
     * @param param        param of query
     * @param resClass     class type of return object.
     * @param cacheKey     cache key
     * @return <R>
     */
    public List<R> query(Function<P, List<R>> queryDB, P param, Class<R> resClass, String cacheKey) {
        // 1. first query cache.
        String jsonStr = redisTemplate.opsForValue().get(cacheKey);
        if (jsonStr != null) {
            // 1.1 if existed,return result directly.
            return JSONUtil.toList(jsonStr,resClass);
        }
        // 1.2 if not exist,query database and write to cache
        List<R> data = queryDB.apply(param);
        redisTemplate.opsForValue().set(cacheKey, JSONUtil.toJsonStr(data), CACHE_EXPIRED_TIME, CACHE_EXPIRED_TIME_UNIT);
        return data;
    }

    /**
     * For query add cache.
     *
     * @param queryDB      query function
     * @param resClass     class type of return object.
     * @param cacheKey     cache key
     * @return <R>
     */
    public List<R> query(Supplier<List<R>> queryDB, Class<R> resClass, String cacheKey) {
        // 1. first query cache.
        String jsonStr = redisTemplate.opsForValue().get(cacheKey);
        if (jsonStr != null) {
            // 1.1 if existed,return result directly.
            return JSONUtil.toList(jsonStr,resClass);
        }
        // 1.2 if not exist,query database and write to cache
        List<R> data = queryDB.get();
        redisTemplate.opsForValue().set(cacheKey, JSONUtil.toJsonStr(data), CACHE_EXPIRED_TIME, CACHE_EXPIRED_TIME_UNIT);
        return data;
    }
}
