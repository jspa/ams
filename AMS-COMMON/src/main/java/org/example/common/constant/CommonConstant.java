package org.example.common.constant;

public class CommonConstant {
    /**
     * 模块类型
     */
    public static final String ORDER_MODULE = "order";

    /********************* 订单模块常量 ************************/
    public static final String STORE_IS_NULL = "库存不足!";
    public static final String ORDER_SUCCESS = "下单成功!";

    /**
     * 订单状态
     */
    public static final Integer ORDER_STATUS_PAYED = 1;
    public static final Integer ORDER_STATUS_NO_PAY = 0;
    public static final Integer ORDER_STATUS_CANCEL = 2;

    /*********************************************************/

}
