package org.example.common.constant;

public class MQConstant {

    /**
     * 投递中
     */
    public static final Integer DELIVERING = 0;

    /**
     * 投递成功
     */
    public static final Integer SUCCESS = 1;

    /**
     * 投递失败
     */
    public static final Integer FAILURE = 2;


    /**
     * 最大尝试投递次数
     */
    public static final Integer MAX_TRY_COUNT = 5;

    /**
     * 消息超时时间 1分钟
     */
    public static final Integer MSG_TIMEOUT = 1;

    public static final String ORDER_QUEUE_NAME = "order.queue";

    public static final String ORDER_EXCHANGE_NAME = "order.exchange.direct";

    public static final String ORDER_ROUTING_KEY = "order.routing.key";
}
