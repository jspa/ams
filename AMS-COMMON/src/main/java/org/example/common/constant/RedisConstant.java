package org.example.common.constant;

import java.util.concurrent.TimeUnit;

/**
 * redis 常量文件
 * @author cheval
 */
public class RedisConstant {


    // ******************************  系统管理 模块开始 ******************************** /

    /** 导航菜单key */
    public static final String NAV_TREE_KEY = "system:auth:nav_tree";
    public static final Integer NAV_TREE_CACHE_TIME = 5;
    public static final TimeUnit NAV_TREE_CACHE_TIME_UNIT = TimeUnit.MINUTES;

    /** 菜单树key */
    public static final String MENU_TREE_KEY = "system:auth:menu_tree";
    public static final Integer MENU_TREE_CACHE_TIME = 10;
    public static final TimeUnit MENU_TREE_CACHE_TIME_UNIT = TimeUnit.MINUTES;

    /** 字典key前缀 */
    public static final String DICT_TYPE_SUB = "system:dict:";
    public static final Integer DICT_CACHE_TIME = 1;
    public static final TimeUnit DICT_CACHE_TIME_UNIT = TimeUnit.DAYS;

    /** 用户信息key */
    public static final String USERS_INFO_KEY = "system:userinfo:users";
    public static final Integer USERS_INFO_CACHE_TIME = 1;
    public static final TimeUnit USERS_INFO_CACHE_TIME_UNIT = TimeUnit.DAYS;

    /** 分布式锁    锁名称为lock:模块名:业务名 */
    public static final String LOCK_PRE = "lock:";
    public static final Integer LOCK_LEASE_TIME = 5;
    public static final TimeUnit LOCK_LEASE_TIME_UNIT = TimeUnit.SECONDS;
    /** 单位s */
    public static final Integer MAX_WAIT_LOCK_TIME = 15;


    /** 签到数据前缀 */
    public static final String SING_DATA_KEY_PREFIX = "system:sign_data:";


    /** 登录用户信息key */
    public static final String LOGIN_USER_INFO_KEY_PREFIX = "system:login_user_info:";
    public static final Integer LOGIN_USER_INFO_CACHE_TIME = 30;
    public static final TimeUnit LOGIN_USER_INFO_CACHE_TIMEUNIT = TimeUnit.MINUTES;
    // ******************************  系统管理  模块结束 ******************************** /





}
