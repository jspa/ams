package org.example.common.utils;

import org.springframework.data.redis.core.RedisTemplate;

import java.time.LocalDate;

/**
 * @author cheval
 */
public class IDGenerate {

    private RedisTemplate redisTemplate;

    public IDGenerate(RedisTemplate redisTemplate) {
        this.redisTemplate = redisTemplate;
    }

    /**
     * 时间戳 + 序列号
     * @param business
     * @return
     */
    public Long nextId(String business) {
        Long beginTimestamp = 1695632171336L;
        Long gutter = System.currentTimeMillis() - beginTimestamp;
        LocalDate.now().toString();
        String IDCounterKey = "IDCounter:" + business + ":" + LocalDate.now().toString();
        Long sequence = redisTemplate.opsForValue().increment(IDCounterKey) ;
        return gutter << 32 | sequence;
    }
}
