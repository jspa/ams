package org.example.common.utils;


/**
 * 通用算法
 * @author cheval
 */
public class CommonAlgorithm {


    /**
     * 判断某一年是否是闰年
     * 荣年的两种情况：
     *  1. 既可以被100整除又可以被400整除的数
     *  2. 不可以被100整除，但是可以被4整除的数
     * @param year
     * @return
     */
    public static boolean isLeapYear(int year) {
        if (year % 100 == 0) {
            if (year % 400 == 0) {
                return true;
            } else {
                return false;
            }
        } else {
            if (year % 4 == 0) {
                return true;
            } else {
                return false;
            }
        }
    }
}
