package org.example.common.thirdframe.minio;

import io.minio.*;
import io.minio.errors.MinioException;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;

public class FileUploader {
    public static void main(String[] args)
            throws IOException, NoSuchAlgorithmException, InvalidKeyException {
//        docker run -p 9000:9000 --name minio -d --restart=always -e "MINIO_ROOR_USER=minio" -e "MINIO_ROOT_PASSWORD=minio123"   -v /home/data:/home/docker/minio/data -v /home/config/:/home/docker/minio/config minio/minio server /data  --console-address ":9000" -address ":9090"

        try {
            // Create a minioClient with the MinIO server playground, its access key and secret key.
            MinioClient minioClient =
                    MinioClient.builder()
                            .endpoint("http://192.168.0.157:9000")  // 这里要客户端端口
                            .credentials("minio", "minio123")
                            .build();

            // Make 'asiatrip' bucket if not exist.
            boolean found =
                    minioClient.bucketExists(BucketExistsArgs.builder().bucket("ams").build());
            if (!found) {
                // Make a new bucket called 'asiatrip'.
                minioClient.makeBucket(MakeBucketArgs.builder().bucket("ams").build());
            } else {
                System.out.println("Bucket 'ams' already exists.");
            }

            // Upload '/home/user/Photos/asiaphotos.zip' as object name 'asiaphotos-2015.zip' to bucket
            // 'asiatrip'.
            File file = new File("C:\\Users\\cheva\\Desktop\\1.txt");
            FileInputStream in  = new FileInputStream(file);
            minioClient.putObject(
                    PutObjectArgs.builder()
                            .bucket("ams")
                            .object("1.txt")
                            .stream(in,1024,-1)
                            .build());
//            minioClient.uploadObject(
//                    UploadObjectArgs.builder()
//                            .bucket("ams")
//                            .object("1.txt")
//                            .filename("/home/user/Photos/asiaphotos.zip")
//                            .build());
            System.out.println("上传成功");
        } catch (MinioException e) {
            System.out.println("Error occurred: " + e);
            System.out.println("HTTP trace: " + e.httpTrace());
        }
    }
}