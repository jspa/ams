# AMS

#### 介绍
权限控制系统，就要包含权限控制、前后端代码生成、定时任务管理、系统日志管理、字典管理等，目前仍然处于开发中。

#### 软件架构
软件架构说明
vue前端 - java后端  -  缓存  -  数据库

![输入图片说明](https://foruda.gitee.com/images/1697007327048075092/c9121fff_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007364220956973/2ef8d158_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007389377997853/2457e38f_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007416242307062/421319d6_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007452647664959/071342b0_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007473210720650/ca171349_9130440.png "屏幕截图")

![输入图片说明](https://foruda.gitee.com/images/1697007494709845049/ee543e1c_9130440.png "屏幕截图")

#### 安装教程

前端项目安装：
1. 下载代码到本地。
2. 修改后端结构根路径。
![输入图片说明](https://foruda.gitee.com/images/1697007591841525751/fa9a4aec_9130440.png "屏幕截图")
3.  cmd窗口进入项目目录安装项目依赖。
npm install

后端项目安装：
安装mysql redis 
修改yml配置。


#### 使用说明

1.  xxxx
2.  xxxx
3.  xxxx

#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 特技

1.  使用 Readme\_XXX.md 来支持不同的语言，例如 Readme\_en.md, Readme\_zh.md
2.  Gitee 官方博客 [blog.gitee.com](https://blog.gitee.com)
3.  你可以 [https://gitee.com/explore](https://gitee.com/explore) 这个地址来了解 Gitee 上的优秀开源项目
4.  [GVP](https://gitee.com/gvp) 全称是 Gitee 最有价值开源项目，是综合评定出的优秀开源项目
5.  Gitee 官方提供的使用手册 [https://gitee.com/help](https://gitee.com/help)
6.  Gitee 封面人物是一档用来展示 Gitee 会员风采的栏目 [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
