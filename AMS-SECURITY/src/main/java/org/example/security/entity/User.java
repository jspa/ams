package org.example.security.entity;

import lombok.Data;

import java.util.Set;

@Data
public class User {
    private String username;
    private Set<String> permissions;
}
