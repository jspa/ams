package org.example.security.autoconfiguration;

import io.jsonwebtoken.SignatureAlgorithm;
import lombok.Data;
import org.springframework.boot.context.properties.ConfigurationProperties;

import java.util.List;

@Data
@ConfigurationProperties(prefix = "security")
public class SecurityProperties {
    private JWTProperties jwt = new JWTProperties();
    private List<String> ignoreUrls;

    @Data
    @ConfigurationProperties("security.jwt")
    public class JWTProperties {
        private String authorizationHeaderName = "Authorization";
        private String bearer = "Bearer";
        private String secret = "secret";
        private Long expiration = 30L; // 过期时间 以分钟为单位
        private SignatureAlgorithm signatureAlgorithm = SignatureAlgorithm.HS256;  // 加密算法
        private String loginUserInfoKey = "userDetails";
        private String loginUsernameKey = "username";
        private String loginUserPermissionKey = "authorities";
    }
}
