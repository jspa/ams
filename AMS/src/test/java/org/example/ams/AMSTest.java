package org.example.ams;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.data.redis.connection.BitFieldSubCommands;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.test.context.junit4.SpringRunner;

import java.math.BigDecimal;
import java.text.DecimalFormat;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AMSTest {

    @Autowired
    private StringRedisTemplate redisTemplate;

    @Test
    public void testBitmap1() {
        redisTemplate.opsForValue().setBit("test",28,true);
    }

    @Test
    public void testBitmap2() {
        List<Long> longs = redisTemplate.opsForValue().bitField("test", BitFieldSubCommands.create()
                .get(BitFieldSubCommands.BitFieldType.unsigned(29)).valueAt(0));

        System.out.println(longs);
    }

    public static void main(String[] args) {
        DecimalFormat format = new DecimalFormat("#.00");
        BigDecimal a = new BigDecimal(123.234);
        System.out.println(format.format(a));
    }
}
