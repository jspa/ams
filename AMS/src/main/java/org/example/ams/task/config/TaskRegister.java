package org.example.ams.task.config;

import org.springframework.beans.factory.DisposableBean;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.config.CronTask;
import org.springframework.stereotype.Component;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ScheduledFuture;

/**
 * 定时任务注册 删除
 * @author cheval
 */
@Component
public class TaskRegister implements DisposableBean {
    /**
     * 任务调度器线程池
     */
    @Autowired
    @Qualifier("taskScheduler")
    private TaskScheduler taskScheduler;

    /**
     * 定时任务队列,记录已经启动的定时任务，如果不加该项，删除的时候就不知道要删除任务的详细信息了
     * ScheduledFuture : 这个是启动定时任务返回的任务结果（任务对象）
     */
    private final Map<String, ScheduledFuture> taskLists = new HashMap();

    /**
     * 添加定时任务
     * @param task
     * @param cron
     */
    public void registerTask(SchedulingTask task,String cron) {
        CronTask cronTask = new CronTask(task,cron);
        ScheduledFuture<?> schedule = taskScheduler.schedule(task, cronTask.getTrigger());
        // 将添加的定时任务注册到任务队列中
        if (taskLists.containsKey(task.getScheduledId())) {
            // 如果已经存在一个当前任务，则先移除
            removeTask(task.getScheduledId());
        }
        taskLists.put(task.getScheduledId(),schedule);
    }

    /**
     * 删除定时任务
     * @param scheduled
     */
    public void removeTask(String scheduled) {
        ScheduledFuture scheduledFuture = taskLists.remove(scheduled);
        if (scheduledFuture != null) {
            scheduledFuture.cancel(true);
        }
    }

    /**
     * 该bean销毁之前，中断所有定时任务
     * @throws Exception
     */
    @Override
    public void destroy() throws Exception {
        if (taskLists.size() > 0) {
            for (ScheduledFuture future : taskLists.values()) {
                future.cancel(true); // 中断定时任务线程
            }
            taskLists.clear(); // 清空定时任务队列
        }
    }
}
