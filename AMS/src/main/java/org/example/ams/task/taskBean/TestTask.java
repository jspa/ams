package org.example.ams.task.taskBean;

import org.example.ams.task.annotation.Task;
import org.example.ams.task.annotation.TaskGroup;
import org.springframework.stereotype.Component;

@TaskGroup(taskGroupName = "测试定时任务")
@Component
public class TestTask extends AbstractTask {

    @Task(taskId = "@test",taskName = "测试1")
    public void test1() {
    }

    @Task(taskId = "@test1",taskName = "测试2")
    public void test2() {
    }
}
