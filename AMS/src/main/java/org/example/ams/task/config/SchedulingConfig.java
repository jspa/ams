package org.example.ams.task.config;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.TaskScheduler;
import org.springframework.scheduling.concurrent.ThreadPoolTaskScheduler;

/**
 * 定时任务配置类
 * @author cheval
 */
@Configuration
public class SchedulingConfig {

    /**
     * 任务调度器
     * 本质是一个定时任务线程池ScheduledExecutorService
     * @return
     */
    @Bean
    public TaskScheduler taskScheduler() {
        ThreadPoolTaskScheduler taskScheduler = new ThreadPoolTaskScheduler();
        // 核心线程数,设置为当前cpu核心数
        taskScheduler.setPoolSize(Runtime.getRuntime().availableProcessors());
        // 中断当前任务后会将其从任务等待队列（如果队列中有该任务）中移除
        taskScheduler.setRemoveOnCancelPolicy(true);
        // 设置线程名前缀
        taskScheduler.setThreadNamePrefix("TaskSchedulerThreadPool-");
        return taskScheduler;
    }
}
