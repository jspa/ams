package org.example.ams.task.taskBean;

import org.example.ams.service.system.ISysLogService;
import org.example.ams.service.system.ISysScheduledService;
import org.example.ams.task.annotation.Task;
import org.example.ams.task.annotation.TaskGroup;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;


@TaskGroup(taskGroupName = "系统日志管理定时任务")
@Component
public class LogTask extends AbstractTask {

    @Autowired
    private ISysScheduledService sysScheduledService;
    @Autowired
    private ISysLogService sysLogService;


    /**
     * 初始化定时任务到数据库
     */
    /*@PostConstruct
    public void init() {
        // 判断数据库中是否已经存在该任务
        SysScheduled sysScheduled = sysScheduledService.queryById(scheduled.getId());
        if (sysScheduled != null) {
            return;
        }
        // 没有则初始化
        sysScheduled = new SysScheduled();
        sysScheduled.setId(scheduled.getId());
        sysScheduled.setScheduledName("清除过期日志");
        sysScheduled.setCron("0 0 1 * * ?"); // 初始默认每天1点执行一次
        sysScheduled.setStatus(false);
        sysScheduledService.save(sysScheduled);
    }*/

    /**
     * 清理方法
     */
    @Task(taskId = "@clearLog",taskName = "定时清理系统日志")
    public void scheduleClearLog() {
        sysLogService.clearLog();
    }
}
