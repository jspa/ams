package org.example.ams.task;

import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Component;

import javax.annotation.PostConstruct;
import java.util.concurrent.ScheduledExecutorService;
import java.util.concurrent.ScheduledThreadPoolExecutor;
import java.util.concurrent.TimeUnit;

/**
 * @author
 */
@Slf4j
//@Component
public class TestScheduleExecutorService {
    private ScheduledThreadPoolExecutor executorService;

    public TestScheduleExecutorService() {
        this.executorService =  new ScheduledThreadPoolExecutor(10);
        executorService.setRemoveOnCancelPolicy(true);
    }

    @PostConstruct
    public void test() throws InterruptedException {
        Runnable task = () -> {
            log.info("定时任务。。。。。。。。。。。。。。。");
        };
        // 添加定时任务
        executorService.scheduleWithFixedDelay(task,0,10, TimeUnit.SECONDS);
        Thread.sleep(500*10);
        // 移除定时任务
        executorService.remove(task);
    }
}
