package org.example.ams.service.system;


import io.swagger.models.auth.In;
import org.example.ams.entity.system.SysUser;

import java.util.List;
import java.util.Map;

/**
 * @author cheval
 */
public interface ISysUserService {
    /**
     * 查询用户下信息
     * @param currentPage
     * @param pageSize
     * @param username
     * @return
     */
    Map<String,Object> queryUserByPage(Integer currentPage, Integer pageSize, String username);

    /**
     * 添加系统用户
     * @param sysUser
     * @return
     */
    Integer addUser(SysUser sysUser);

    /**
     * 修改用户状态id
     * @param userId
     */
    void updateStatus(Integer userId);

    /**
     * 查询所有
     * @return
     */
    List<SysUser> queryAll();

    /**
     * 根据用户名查询
     * @param systemDefaultUserName
     * @return
     */
    SysUser queryByUsername(String systemDefaultUserName);

    /**
     * 修改用户
     * @param user
     */
    void update(SysUser user);

    /**
     * 根据id批量删除
     * @param ids
     */
    void deleteByIds(Integer[] ids);
}
