package org.example.ams.service.system;

import org.example.ams.entity.system.SysLog;
import org.example.common.api.CommonResult;

import java.util.Date;
import java.util.Map;

public interface ISysLogService {
    void save(SysLog sysLog);


//    CommonResult<Map<String, Object>> queryByPage(Integer currentPage, Integer pageSize, String user, Date[] optionDate);

    /**
     * 清除过期日志
     */
    void clearLog();

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 页大小
     * @param params 查询参数
     * @return  Map<String,Object>
     */
    Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params);


    /**
     * 删除
     * @param ids ids
     */
    void delete(Integer[] ids);
}
