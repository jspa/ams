package org.example.ams.service.system;

import org.example.ams.entity.system.SysDictType;

import java.util.Map;

/**
 * @author cheval
 */
public interface ISysDictTypeService {
    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 页大小
     * @param params 查询参数
     * @return  Map<String,Object>
     */
    Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params);

    /**
     * 添加
     * @param sysDictType 添加对象
     */
    void save(SysDictType sysDictType);

    /**
     * 修改
     * @param sysDictType 修改对象
     */
    void update(SysDictType sysDictType);

    /**
     * 删除
     * @param ids ids
     */
    void delete(Integer[] ids);

    /**
     * 通过字典类型查询
     * @param dictType
     * @return
     */
    SysDictType queryByType(String dictType);
}
