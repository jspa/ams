package org.example.ams.service.system;



import org.example.ams.entity.system.SysDictData;

import java.util.List;
import java.util.Map;

public interface ISysDictDataService {
    Map<String, List<SysDictData>> queryDictDataByDictType(String dictType);

    /**
     * 更新
     * @param sysDictData
     */
    void update(SysDictData sysDictData);

    /**
     * 添加
     * @param sysDictData
     */
    void add(SysDictData sysDictData);

    /**
     * 删除
     * @param sysDictData
     */
    void delete(SysDictData sysDictData);

    /**
     * 根据字典类型删除字典项
     * @param typeId
     */
    void deleteByType(Integer[] typeIds);
}
