package org.example.ams.service.system;


import org.example.ams.entity.system.SysMenu;

import java.util.List;
import java.util.Map;

public interface ISysMenuService {
    /**
     * 获取导航菜单
     * 条件：
     * 1）当前登录用户拥有的菜单
     * 2）菜单类型为 1/2（目录和菜单）
     * 3）未被删除 is_validate = 1
     * 4）未被禁用 enable = 1
     * 5）树形结构
     */
    List<SysMenu> queryMenuTree();

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 页大小
     * @param params 查询参数
     * @return  Map<String,Object>
     */
    Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params);

    /**
     * 添加
     * @param sysMenu 添加对象
     */
    void save(SysMenu sysMenu);

    /**
     * 修改
     * @param sysMenu 修改对象
     */
    void update(SysMenu sysMenu);

    /**
     * 删除
     * @param ids ids
     */
    void delete(Integer[] ids);

    /**
     * 查询所有目录
     */
    List<SysMenu> queryAllDirectory();

    /**
     * 检查ID是否已经存在
     * @param menuId
     * @return
     */
    boolean checkIDExist(Integer menuId);

    /**
     * 查询所有权限
     * @return
     */
    List<SysMenu> queryAuthTree();
}
