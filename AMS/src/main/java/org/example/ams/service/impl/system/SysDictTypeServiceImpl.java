package org.example.ams.service.impl.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.example.ams.service.system.ISysDictDataService;
import org.example.ams.service.system.ISysDictTypeService;
import org.example.ams.util.SystemUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.example.ams.dao.system.ISysDictTypeDao;
import org.example.ams.entity.system.SysDictType;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;


@Service
public class SysDictTypeServiceImpl implements ISysDictTypeService {
    @Autowired
    private ISysDictTypeDao sysDictTypeDao;
    @Autowired
    private SystemUtils loginContextUtils;
    @Autowired
    private ISysDictDataService sysDictDataService;

    /**
     * 分页查询
     *
     * @param currentPage
     * @param pageSize
     * @param params
     * @return
     */
    @Override
    public Map<String, Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params) {
        QueryWrapper<SysDictType> queryWrapper = new QueryWrapper<SysDictType>();


        queryWrapper = queryWrapper.orderByAsc("id");

        if (params.get("dictName") != null && !"".equals(params.get("dictName"))) {
            queryWrapper = queryWrapper.like("dict_name", params.get("dictName")[0]);
        }


        if (params.get("dictType") != null && !"".equals(params.get("dictType"))) {
            queryWrapper = queryWrapper.like("dict_type", params.get("dictType")[0]);
        }


        Page<SysDictType> page = new Page<SysDictType>(currentPage, pageSize);
        Page<SysDictType> ipage = null;
        ipage = sysDictTypeDao.selectPage(page, queryWrapper);
        // 数据转化
        Map<Integer, String> userIdToNameMapper = loginContextUtils.getUserIdToNameMapper();
        List<SysDictType> types = ipage.getRecords();
        for (SysDictType record : types) {
            record.setCreateUserName(userIdToNameMapper.get(record.getCreateUserId()));
            record.setUpdateUserName(userIdToNameMapper.get(record.getUpdateUserId()));
        }
        Map<String, Object> res = new HashMap<>();
        res.put("total", ipage.getTotal());
        res.put("data", types);
        return res;
    }

    /**
     * 添加
     *
     * @param sysDictType 添加对象
     */
    @Override
    public void save(SysDictType sysDictType) {
        sysDictType.setStatus(1);
        sysDictType.setCreateTime(new Date());
        sysDictType.setUpdateTime(new Date());
        sysDictType.setCreateUserId(loginContextUtils.loginUser().getUserId());
        sysDictType.setUpdateUserId(loginContextUtils.loginUser().getUserId());
        sysDictTypeDao.insert(sysDictType);
    }

    /**
     * 修改
     *
     * @param sysDictType 修改对象
     */
    @Override
    public void update(SysDictType sysDictType) {
        sysDictTypeDao.updateById(sysDictType);
    }

    /**
     * 删除
     *
     * @param ids ids
     */
    @Transactional(rollbackFor = Exception.class)
    @Override
    public void delete(Integer[] ids) {
        // 先删除字典项，后删除字典类型
        if (ids != null && ids.length > 0) {
            sysDictDataService.deleteByType(ids);
            sysDictTypeDao.deleteBatchIds(Arrays.asList(ids));
            // 字典项缓存让其自动过期即可
        }
    }

    @Override
    public SysDictType queryByType(String dictType) {
        return sysDictTypeDao.selectOne(new QueryWrapper<SysDictType>().eq("dict_type",dictType));
    }
}
