package org.example.ams.service.system;



public interface ISysUserRoleService {
    /**
     * 给用户添加角色
     * @param userId
     * @param roleIds
     */
    void add(Integer userId, Integer[] roleIds);

    /**
     * 删除用户拥有的所有角色
     * @param userId
     */
    void deleteAll(Integer userId);

    /**
     * 删除用户角色
     * @param userId
     * @param roleId
     */
    void deleteByUserIdAndRoleId(Integer userId, Integer roleId);
}
