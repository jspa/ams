package org.example.ams.service.impl.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import org.example.ams.dao.system.ISysDictDataDao;
import org.example.ams.dao.system.ISysDictTypeDao;
import org.example.ams.entity.system.SysDictData;
import org.example.ams.entity.system.SysDictType;
import org.example.ams.service.system.ISysDictDataService;
import org.example.ams.util.SystemUtils;
import org.example.common.cache.CacheUtils;
import org.example.common.constant.RedisConstant;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.scheduling.concurrent.ThreadPoolTaskExecutor;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.*;

@Service
@Transactional
public class SysDictDataServiceImpl implements ISysDictDataService {

    @Autowired
    private ISysDictDataDao sysDictDataDao;
    @Autowired
    private ISysDictTypeDao sysDictTypeDao;
    @Autowired
    private StringRedisTemplate redisTemplate;
    @Autowired
    private ThreadPoolTaskExecutor threadPoolTaskExecutor;
    @Autowired
    private SystemUtils systemUtils;

    @Override
    public Map<String, List<SysDictData>> queryDictDataByDictType(String dictType) {
        Map<String, List<SysDictData>> map = new HashMap<>();
        // 先从缓存中获取，如果没有则从数据库获取并且存入缓存中
        String key = RedisConstant.DICT_TYPE_SUB + dictType;
        List<SysDictData> sysDictDatas = null;
        CacheUtils<String, SysDictData> cacheUtils = new CacheUtils<>(redisTemplate,threadPoolTaskExecutor);
        sysDictDatas = cacheUtils.mutexQuery((type) -> {
            return sysDictDataDao.selectByDictType(type);
        }, dictType, "dict", "dict_data", SysDictData.class, key);

        // 数据处理
        Map<Integer, String> userIdToNameMapper = systemUtils.getUserIdToNameMapper();
        for (SysDictData sysDictData : sysDictDatas) {
            sysDictData.setCreateUserName(userIdToNameMapper.get(sysDictData.getCreateUserId()));
            sysDictData.setUpdateUserName(userIdToNameMapper.get(sysDictData.getUpdateUserId()));
        }
        map.put(dictType,sysDictDatas);
        return map;
    }

    @Override
    public void update(SysDictData sysDictData) {
        SysDictType sysDictType = sysDictTypeDao.selectById(sysDictData.getType());
        // 更新
        sysDictDataDao.updateById(sysDictData);
        // 删除缓存
        String key = RedisConstant.DICT_TYPE_SUB + sysDictType.getDictType();
        redisTemplate.delete(key);
    }

    @Override
    public void add(SysDictData sysDictData) {
        SysDictType sysDictType = sysDictTypeDao.selectById(sysDictData.getType());
        sysDictData.setCreateTime(new Date());
        sysDictData.setUpdateTime(new Date());
        sysDictData.setCreateUserId(systemUtils.loginUser().getUserId());
        sysDictData.setUpdateUserId(systemUtils.loginUser().getUserId());
        if (sysDictData.getIsDefault() == null) {
            sysDictData.setIsDefault(false);
        }
        sysDictData.setStatus(true);
        if (sysDictData.getOrderNum() == null) {
            sysDictData.setOrderNum(0);
        }
        // 添加
        sysDictDataDao.insert(sysDictData);
        // 删除缓存
        String key = RedisConstant.DICT_TYPE_SUB + sysDictType.getDictType();
        redisTemplate.delete(key);
    }

    @Override
    public void delete(SysDictData sysDictData) {
        // 删除
        sysDictDataDao.deleteById(sysDictData.getId());
        // 删除缓存
        SysDictType sysDictType = sysDictTypeDao.selectById(sysDictData.getType());
        String key = RedisConstant.DICT_TYPE_SUB + sysDictType.getDictType();
        redisTemplate.delete(key);
    }

    @Override
    public void deleteByType(Integer[] typeIds) {
        sysDictDataDao.delete(new QueryWrapper<SysDictData>().in("dict_type_id", Arrays.asList(typeIds)));
    }
}
