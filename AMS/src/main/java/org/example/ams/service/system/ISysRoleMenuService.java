package org.example.ams.service.system;

import org.example.ams.entity.system.SysRoleMenu;

import java.util.List;

/**
 * @author cheval
 */
public interface ISysRoleMenuService {
    /**
     * 查询角色拥有的权限
     * @param roleId
     * @return
     */
    List<SysRoleMenu> queryByRoleId(Integer roleId);

    /**
     * 更新角色的权限，先删除再插入
     * @param roleId
     * @param menuIds
     * @return
     */
    boolean updateAuth(Integer roleId, Integer[] menuIds);
}
