package org.example.ams.service.generator;

import org.example.ams.entity.generator.TableFields;
import org.example.ams.entity.system.SysMenu;

import java.util.List;

/**
 * @author cheval
 */
public interface ICodeGeneratorService {

    /**
     * 查询系统所有表名
     * @return
     */
    List<String> queryAllTables();

    List<TableFields> queryTableFields(String tableName);
}
