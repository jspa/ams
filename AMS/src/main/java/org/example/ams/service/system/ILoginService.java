package org.example.ams.service.system;


import org.example.ams.entity.system.SysUser;
import org.example.ams.vo.LoginUser;
import org.example.common.api.CommonResult;

public interface ILoginService {

    CommonResult login(LoginUser user);

    SysUser querySysUserByUserName(String username);
}
