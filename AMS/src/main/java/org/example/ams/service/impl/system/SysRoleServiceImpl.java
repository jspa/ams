package org.example.ams.service.impl.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.example.ams.dao.system.ISysRoleDao;
import org.example.ams.entity.system.SysRole;
import org.example.ams.service.system.ISysRoleService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

@Service
public class SysRoleServiceImpl implements ISysRoleService {
    @Autowired
    private ISysRoleDao sysRoleDao;

    @Override
    public List<SysRole> queryNoHavRoleForUser(Integer userId) {
        return sysRoleDao.selectNoHavRoleForUser(userId);
    }

    /**
     * 分页查询
     * @param currentPage
     * @param pageSize
     * @param params
     * @return
     */
    @Override
    public Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params){
        QueryWrapper<SysRole> queryWrapper = new QueryWrapper<SysRole>();

        if (params.get("roleName") != null && !"".equals(params.get("roleName"))) {
            queryWrapper = queryWrapper.like("role_name",params.get("roleName")[0]);
        }
        queryWrapper = queryWrapper.orderByAsc("role_code");

        Page<SysRole> page = new Page<SysRole>(currentPage,pageSize);
        Page<SysRole> ipage = null;
        ipage = sysRoleDao.selectPage(page, queryWrapper);
        Map<String,Object> res = new HashMap<>();
        res.put("total",ipage.getTotal());
        res.put("data",ipage.getRecords());
        return res;
    }

    /**
     * 添加
     * @param sysRole 添加对象
     */
    @Override
    public void save(SysRole sysRole) {
        sysRoleDao.insert(sysRole);
    }

    /**
     * 修改
     * @param sysRole 修改对象
     */
    @Override
    public void update(SysRole sysRole) {
        sysRoleDao.updateById(sysRole);
    }

    /**
     * 删除
     * @param ids ids
     */
    @Override
    public void delete(Integer[] ids) {
        sysRoleDao.deleteBatchIds(Arrays.asList(ids));
    }

    /**
     * 更改状态
     * @param id
     * @return
     */
    @Override
    public boolean updateStatus(Integer id) {
        int row = sysRoleDao.updateStatus(id);
        if (row == 1) {
            return true;
        }
        return false;
    }
}
