package org.example.ams.service.impl.system;

import com.baomidou.mybatisplus.core.conditions.query.QueryWrapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.example.ams.dao.system.ISysLogDao;
import org.example.ams.entity.system.SysLog;
import org.example.ams.service.system.ISysLogService;
import org.example.ams.util.SystemUtils;
import org.example.common.api.CommonResult;
import org.example.common.utils.IpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Service;

import java.util.*;

@Service
public class SysLogServiceImpl implements ISysLogService {
    @Autowired
    private ISysLogDao sysLogDao;
    @Autowired
    private SystemUtils systemUtils;
    @Value("${option-log-save-time}")
    private Integer logSaveTime = 30;

    /**
     * 分页查询
     * @param currentPage
     * @param pageSize
     * @param params
     * @return
     */
    @Override
    public Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params){
        QueryWrapper<SysLog> queryWrapper = new QueryWrapper<SysLog>();

        if (params.get("user") != null && !"".equals(params.get("user"))) {
            queryWrapper = queryWrapper.like("user",params.get("user")[0]);
        }

        if (params.get("content") != null && !"".equals(params.get("content"))) {
            queryWrapper = queryWrapper.like("content",params.get("content")[0]);
        }

        if (params.get("optionTime") != null && params.get("optionTime").getClass().isArray()) {
            queryWrapper = queryWrapper.between("option_time",params.get("optionTime")[0],params.get("optionTime")[1]);
        }

        queryWrapper = queryWrapper.orderByDesc("option_time");

        Page<SysLog> page = new Page<SysLog>(currentPage,pageSize);
        Page<SysLog> ipage = null;
        ipage = sysLogDao.selectPage(page, queryWrapper);
        Map<String,Object> res = new HashMap<>();
        res.put("total",ipage.getTotal());
        res.put("data",ipage.getRecords());
        return res;
    }


    @Override
    public void save(SysLog sysLog) {
        sysLogDao.insert(sysLog);
    }

//    @Override
//    public CommonResult<Map<String, Object>> queryByPage(Integer currentPage, Integer pageSize, String user, Date[] optionDate) {
//        Page<SysLog> page = new Page<>(currentPage,pageSize);
//        Page<SysLog> iPage = sysLogDao.selectByPage(page,user,optionDate);
//        Map<String,Object> map = new HashMap<>();
//        map.put("data", iPage.getRecords());
//        map.put("total",iPage.getTotal());
//        return CommonResult.success(map,null);
//    }

    @Override
    public void clearLog() {
        sysLogDao.deleteExpiredLog(logSaveTime);
    }



    /**
     * 删除
     * @param ids ids
     */
    @Override
    public void delete(Integer[] ids) {
        sysLogDao.deleteBatchIds(Arrays.asList(ids));
    }
}
