package org.example.ams.service.system;


import org.example.ams.entity.system.SysScheduled;

import java.util.List;
import java.util.Map;

/**
 * @author cheval
 */
public interface ISysScheduledService {
    /**
     * 根据id查询任务
     * @param scheduledId
     * @return
     */
    SysScheduled queryById(String scheduledId);

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 页大小
     * @param params 查询参数
     * @return  Map<String,Object>
     */
    Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params);

    /**
     * 添加
     * @param sysScheduled
     */
    void save(SysScheduled sysScheduled);

    /**
     * 查询所有已经启动的任务
     * @return
     */
    List<SysScheduled> queryBootstrapedTask();


    /**
     * 更改任务状态
     * @param scheduledId
     * @param status
     * @return
     */
    boolean updateStatus(String scheduledId,Integer status);

    /**
     * 查询所有
     * @return
     */
    List<SysScheduled> list();

    /**
     * 删除任务
     * @param id
     * @return
     */
    int remove(String id);

    /**
     * 添加任务
     * @param sysScheduled
     */
    void addTask(SysScheduled sysScheduled);

    /**
     * 更新任务
     * @param sysScheduled
     * @return
     */
    boolean updateTask(SysScheduled sysScheduled);
}
