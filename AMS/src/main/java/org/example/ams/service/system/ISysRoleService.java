package org.example.ams.service.system;


import org.example.ams.entity.system.SysRole;

import java.util.List;
import java.util.Map;

public interface ISysRoleService {
    List<SysRole> queryNoHavRoleForUser(Integer userId);

    /**
     * 分页查询
     * @param currentPage 当前页码
     * @param pageSize 页大小
     * @param params 查询参数
     * @return  Map<String,Object>
     */
    Map<String,Object> queryByPage(Integer currentPage, Integer pageSize, Map<String, String[]> params);

    /**
     * 添加
     * @param sysRole 添加对象
     */
    void save(SysRole sysRole);

    /**
     * 修改
     * @param sysRole 修改对象
     */
    void update(SysRole sysRole);

    /**
     * 删除
     * @param ids ids
     */
    void delete(Integer[] ids);

    /**
     * 更改状态
     * @param id
     * @return
     */
    boolean updateStatus(Integer id);
}
