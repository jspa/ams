package org.example.ams.entity.generator;

import lombok.Data;

import java.io.Serializable;

/**
 * @author cheval
 */
@Data
public class TemplateParam implements Serializable {
    public static final long serialVersionUID = 42L;

    /** 文件输出路径 */
    private String outPath = "C:\\code";

    /** 包名  必须*/
    private String packageName;

    /** 模块名  必须*/
    private String module;

    /** 表名 */
    private String tableName;

    /** 实体类名 必须*/
    private String className;

    /** 是否生成前端代码 */
    private Boolean isVueCode = false;
}
