package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;
import java.util.List;
import java.util.Set;

@Data
@Accessors(chain = true)
public class SysUser implements Serializable {
    private static final long serialVersionUID = 1L;
    @TableId(type = IdType.AUTO)
    private Integer userId;
    private String userCode;
    private String username;
    private String password;
    private String nameZh;
    private String phone;
    private Integer sex; // 1 男 0 女
    private Boolean status;
    private Boolean isValid;
    private Integer createUserId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private String remark;
    private String img;
    private Boolean isAdmin;
    @TableField(exist = false)
    private Set<String> permissions;
    @TableField(exist = false)
    private List<SysRole> roles;

    @TableField(exist = false)
    private String createUsername;
}
