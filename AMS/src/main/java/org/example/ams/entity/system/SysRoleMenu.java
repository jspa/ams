package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableId;

import java.io.Serializable;

/**
 * @author cheval
 */
@Data
public class SysRoleMenu implements Serializable {
    private static final long serialVersionUID = 1L;

    @TableId(type = IdType.AUTO)
    private Long id;

    private Integer roleId;

    private Integer menuId;
}
