package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableId;
import lombok.Data;
import lombok.experimental.Accessors;

@Data
@Accessors(chain = true)
public class SysUserRole {
    // 不加插入时会报错，因为mybatis-plus默认会有一个id生成策略（字符串），数据库类型是int,就会报类型不匹配。
    // IdType.AUTO 就代表数据库自动生成
    @TableId(value = "id",type = IdType.AUTO)
    private Integer id;
    private Integer userId;
    private Integer roleId;
}
