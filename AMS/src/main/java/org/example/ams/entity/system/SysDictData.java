package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class SysDictData implements Serializable {
    private static final long serialVersionUID = 42L;

    @TableId(type = IdType.AUTO)
    private Integer id;
    @TableField("dict_label")
    private String label;
    @TableField("dict_value")
    private String value;
    @TableField("dict_type_id")
    private Integer type;
    private Boolean isDefault = false;
    private Integer orderNum = 0;
    private Boolean status = true;
    private String cssClass;
    private String listClass;
    private Integer createUserId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    private Integer updateUserId;
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    private String remark;

    @TableField(exist = false)
    private String createUserName;

    @TableField(exist = false)
    private String updateUserName;
}
