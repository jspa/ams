package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import lombok.Data;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;

import java.io.Serializable;
import java.util.Date;

/**
 * @author cheval
 */
@Data
public class SysDictType implements Serializable {
    private static final long serialVersionUID = 1L;


    /**
     *
     */
    @TableId(type = IdType.AUTO)
    private Integer id;


    /**
     * 字典名称
     */
    private String dictName;


    /**
     * 字典类型
     */
    private String dictType;


    /**
     * 状态 1 启用 0 禁用
     */
    private Integer status;


    /**
     * 创建用户
     */
    private Integer createUserId;


    /**
     * 创建时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;


    /**
     * 更新用户
     */
    private Integer updateUserId;


    /**
     * 更新时间
     */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;


    /**
     * 备注
     */
    private String remark;

    @TableField(exist = false)
    private String createUserName;

    @TableField(exist = false)
    private String updateUserName;
}
