package org.example.ams.entity.generator;

import lombok.Data;

import java.io.Serializable;

/**
 * @author cheval
 */
@Data
public class TableFields implements Serializable {

    public static final long serialVersionUID = 42L;

    /** 字段名 */
    private String field;

    /** 数据类型 */
    private String type;

    /** 备注 */
    private String comment;

    /** 是否主键，如果是，值为PRI,如果不是，值为null */
    private String key;

    /** 是否自增 ，如果是，值为auto_increment,如果不是值为null */
    private String extra;

    /** 是否可以为空，YES NO */
    private String isNull;

    /** 是否根据该字段生成查询表单 */
    private Boolean searchable = false;

    /** 查询类型 */
    private SearchType searchType;

    /** 是否根据该字段排序 */
    private Boolean isOrder = false;

    /** 排序类型 */
    private OrderType orderType;

    /** 是否展示在表格中 */
    private Boolean isShow = true;

    /** 是否form表单项 */
    private Boolean isForm = false;

    /** 表单项类型 */
    private FormItemType formItemType;
}
