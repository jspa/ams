package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import lombok.Data;
import lombok.experimental.Accessors;

import java.io.Serializable;
import java.util.Date;

@Data
@Accessors(chain = true)
public class SysLog implements Serializable {
    private static final long serialVersionUID = 1L;

    /**  */
    @TableId(type=IdType.AUTO)
    private Integer id;



    /** 操作用户 */
    private String user;



    /** ip */
    private String ip;



    /** 操作内容 */
    private String content;



    /** 参数 */
    private String param;



    /** 操作时间 */
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date optionTime;

    /** ip归属 */
    @TableField(exist = false)
    private String ipAddress;
}
