package org.example.ams.entity.system.workflow;

import lombok.Data;

/**
 * 工作流模型 对应act_re_model表
 * @author cheval
 */
@Data
public class FlowModel {
    /** 模型名称 */
    private String name;
    /** 模型key */
    private String key;
    /** 模型分类 0-MODEL_TYPE_BPMN */
    private String category;
    /** 模型版本 */
    private Integer version;
    /** 模型描述 */
    private String description;
}
