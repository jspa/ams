package org.example.ams.entity.generator;

/**
 * @author cheval
 */
public enum SearchType {
    /** 等值查询 */
    EQUAL_SEARCH,
    /** 模糊查询 */
    FUZZY_QUERY,
    /** 区间查询 */
    INTERVAL_QUERY,
    /** 范围查询 */
    RANGE_QUERY
}
