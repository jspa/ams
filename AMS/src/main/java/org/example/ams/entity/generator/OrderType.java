package org.example.ams.entity.generator;

/**
 * @author cheval
 */
public enum OrderType {
    ASC,
    DESC
}
