package org.example.ams.entity.generator;

/**
 * form item类型
 * @author cheval
 */
public enum FormItemType {
    INPUT,
    SELECT,
    DATEPICKER,
    INPUTNUMBER,
    NUMBERRANGE,
    PASSWORD,
    TIMEPICKER,
    TIMEPICKERRANGE,
    DATEPICKERRANGE,
    DATETIMEPICKER,
    DATETIMEPICKERRANGE,
    CRON
}
