package org.example.ams.entity.system;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonInclude;
import lombok.Data;
import lombok.experimental.Accessors;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

import java.io.Serializable;
import java.util.Date;
import java.util.List;

@Data
@Accessors(chain = true)
public class SysMenu implements Serializable {
    private static final long serialVersionUID = 1L;

    @Id
    @TableId(type = IdType.ASSIGN_ID)
    private Integer menuId;
    @Field(type = FieldType.Integer)
    private Integer parentId;
    @Field(type = FieldType.Text, analyzer = "ik_max_word",searchAnalyzer = "ik_max_word")
    private String name;
    @Field(type = FieldType.Text)
    private String url;
    @Field(type = FieldType.Integer)
    private Integer type;
    @Field(type = FieldType.Text)
    private String icon;
    @Field(type = FieldType.Integer)
    private Integer orderNum;
    @Field(type = FieldType.Integer)
    private Integer createUserId;
    @Field(type = FieldType.Date)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date createTime;
    @Field(type = FieldType.Date)
    @JsonFormat(pattern = "yyyy-MM-dd HH:mm:ss")
    private Date updateTime;
    @Field(type = FieldType.Text)
    private String routeUrl;
    @Field(type = FieldType.Text)
    private String permission;
    @Field(type = FieldType.Boolean)
    private Boolean enable;
    @Field(type = FieldType.Boolean)
    private Boolean isValidate;
    @Field(type = FieldType.Boolean)
    private Boolean isLink;

    @JsonInclude(value = JsonInclude.Include.ALWAYS)
    @TableField(exist = false)
    private List<SysMenu> children;

    @TableField(exist = false)
    private String createUserName;
}
