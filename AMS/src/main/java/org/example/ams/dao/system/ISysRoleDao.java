package org.example.ams.dao.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.ams.entity.system.SysRole;

import java.util.List;

public interface ISysRoleDao extends BaseMapper<SysRole> {
    List<SysRole> selectNoHavRoleForUser(Integer userId);

    /**
     * 改变状态
     * @param id
     * @return
     */
    int updateStatus(Integer id);
}
