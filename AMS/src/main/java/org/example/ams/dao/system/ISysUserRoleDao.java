package org.example.ams.dao.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.ams.entity.system.SysUserRole;

public interface ISysUserRoleDao extends BaseMapper<SysUserRole> {
}
