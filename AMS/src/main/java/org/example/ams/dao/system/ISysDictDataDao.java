package org.example.ams.dao.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.ams.entity.system.SysDictData;

import java.util.List;

public interface ISysDictDataDao extends BaseMapper<SysDictData> {
    List<SysDictData> selectByDictType(String dictType);
}
