package org.example.ams.dao.generator;

import org.example.ams.entity.generator.TableFields;

import java.util.List;

public interface ICodeGeneratorDao {
    List<String> selectAllTables();


    List<TableFields> selectTableFields(String tableName);
}
