package org.example.ams.dao.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.example.ams.entity.system.SysLog;

import java.util.Date;

public interface ISysLogDao extends BaseMapper<SysLog> {
//    Page<SysLog> selectByPage(@Param("page") Page<SysLog> page, @Param("user") String user, @Param("optionDate") Date[] optionDate);

    void deleteExpiredLog(Integer logSaveTime);
}
