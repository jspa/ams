package org.example.ams.dao.system;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.ams.entity.system.SysMenu;

import java.util.List;

public interface ISysMenuDao extends BaseMapper<SysMenu> {

    /**
     * 通过用户名查询
     * @param username
     * @return
     */
    List<SysMenu> selectByUsername(String username);
}
