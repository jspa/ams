package org.example.ams.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.baomidou.mybatisplus.extension.plugins.pagination.Page;
import org.apache.ibatis.annotations.Param;
import org.example.ams.entity.system.SysUser;

public interface ISysUserDao extends BaseMapper<SysUser> {
    /**
     * 根据用户名查询用户及权限
     */
    SysUser selectUserWithPermissionByUsername(String username);

    /**
     * 获取当前最大用户编码
     * @return
     */
    String selectMaxUserCode();

    /**
     * 查询用户及角色
     */
    Page<SysUser> selectUserWithRoles(@Param("page") Page<SysUser> page, @Param("username") String username);

    /**
     * 修改用户状态
     */
    void updateStatus(Integer userId);
}
