package org.example.ams.dao.system;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.example.ams.entity.system.SysRoleMenu;

public interface ISysRoleMenuDao extends BaseMapper<SysRoleMenu> {
}
