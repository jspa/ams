package org.example.ams.es.sample.TestSample;

import javafx.application.Application;
import org.example.ams.es.sample.Student;
import org.example.ams.es.sample.StudentRepository;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;
import java.util.Optional;

@SpringBootTest(classes = Application.class)
public class TestRepository {

    @Autowired
    private StudentRepository studentRepository;

    /**
     * 使用ElasticsearchRepository自带的方法查询
     */
    @Test
    public void searchStu2(){
//        Optional<Student> stu = studentRepository.findById(1);
//        Iterable<Student> stu2 = studentRepository.findAll();
//        System.out.println(stu);
//        System.out.println(stu2);
    }

    /**
     * 使用自定义方法查询
     */
    @Test
    public void searchStu3(){
        List<Student> stus = studentRepository.findStusByName("didiok");
        System.out.println(stus);
    }
}
