package org.example.ams.es.sample;


import org.springframework.data.elasticsearch.repository.ElasticsearchRepository;

import java.util.List;

/**
 * ElasticsearchRepository<T, ID> T：实体类泛型，ID：ES库中索引的主键类型
 * @author cheval
 */
//public interface StudentRepository extends ElasticsearchRepository<Student,Integer> {
public interface StudentRepository  {

    List<Student> findStusByName(String didiok);
}
