package org.example.ams.es.sample;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.elasticsearch.annotations.Document;
import org.springframework.data.elasticsearch.annotations.Field;
import org.springframework.data.elasticsearch.annotations.FieldType;

@Data
@Document(indexName = "student")
public class Student {

    /**
     * @Id 指定文档的_id, 注意id字段是必须的，可以不写注解@Id。
     */
    @Id
    private Integer id;

    @Field(store = true, analyzer = "ik_max_word",type = FieldType.Text)
    private String name;

    @Field(store = true,type = FieldType.Integer )
    private Integer age;

    @Field
    private Boolean isMarried;
}
