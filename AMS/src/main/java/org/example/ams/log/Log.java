package org.example.ams.log;

import java.lang.annotation.*;

@Target({ElementType.METHOD})
@Retention(RetentionPolicy.RUNTIME)
@Documented
public @interface Log {
    /** 日志内容*/
    String value();
    /**
     * 是否记录参数： 0 不记录 1 记录
     */
    int ifRecordParam() default 0;
}
