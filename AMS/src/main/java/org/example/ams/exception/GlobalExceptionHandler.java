package org.example.ams.exception;

import io.jsonwebtoken.MalformedJwtException;
import org.example.common.api.CommonResult;
import org.springframework.security.authentication.BadCredentialsException;
import org.springframework.security.authentication.DisabledException;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;


@RestControllerAdvice
public class GlobalExceptionHandler {

    /**
     * 用户名密码错误异常处理
     * @return CommonResult
     */
    @ExceptionHandler({BadCredentialsException.class, UsernameNotFoundException.class})
    public CommonResult handlerUsernameOrPasswordErrorException() {
        return CommonResult.failed("用户名或密码错误");
    }

    /**
     * 账号被禁用异常处理
     * @return CommonResult
     */
    @ExceptionHandler({DisabledException.class})
    public CommonResult handlerAccountDisabledException() {
        return CommonResult.failed("账号被禁用！");
    }



    /**
     * token无效异常处理
     * @return CommonResult
     */
    @ExceptionHandler({MalformedJwtException.class})
    public CommonResult handlerTokenErrorException() {
        return CommonResult.failed("无效凭证，请重新登录！");
    }


    /**
     * 请求参数异常处理
     * @return CommonResult
     */
    @ExceptionHandler({MissingServletRequestParameterException.class})
    public CommonResult handlerRequestParameterException() {
        return CommonResult.failed("无效的请求参数！");
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    public CommonResult handMethodArgmentNotVaildException(MethodArgumentNotValidException e) {
//        Map<String, String> errMsg = ValidateUtils.getValidateErrMsg(e.getBindingResult());
        return CommonResult.failed("参数校验异常");
    }

    /**
     * 未知异常处理
     * @return CommonResult
     */
    /*@ExceptionHandler({Exception.class})
    public CommonResult handlerUnknowException() {
        return CommonResult.failed("系统未知异常！");
    }*/
}
