package org.example.ams.util;

import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

public class ValidateUtils {
    public static Map<String,String> getValidateErrMsg(BindingResult bindingResult) {
        Map<String,String> errors = new HashMap<>();
        List<FieldError> fieldErrors = bindingResult.getFieldErrors();
        fieldErrors.stream().forEach(item -> {
            String field = item.getField();
            String error = item.getDefaultMessage();
            errors.put(field,error);
        });
        return errors;
    }
}
