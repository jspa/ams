package org.example.ams.util;

import org.springframework.web.context.request.RequestContextHolder;
import org.springframework.web.context.request.ServletRequestAttributes;

import javax.servlet.http.HttpServletRequest;

/**
 * springMvc工具类
 */
public class SpringMvcUtils {

    /**
     * 获取当前请求的request
     * @return
     */
    public static HttpServletRequest getRequest() {
        return ((ServletRequestAttributes)(RequestContextHolder.getRequestAttributes())).getRequest();
    }
}
