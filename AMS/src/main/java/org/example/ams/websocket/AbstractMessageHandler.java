package org.example.ams.websocket;

/**
 * 报文处理器抽象
 * @author cheval
 */
public interface AbstractMessageHandler {
    /**
     * 处理报文
     */
    void handle();
}
