package org.example.ams.websocket;

import io.netty.bootstrap.ServerBootstrap;
import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.nio.NioEventLoopGroup;
import io.netty.channel.socket.nio.NioServerSocketChannel;
import io.netty.channel.socket.nio.NioSocketChannel;
import io.netty.handler.codec.http.HttpObjectAggregator;
import io.netty.handler.codec.http.HttpServerCodec;
import io.netty.handler.codec.http.websocketx.WebSocketServerProtocolHandler;
import io.netty.handler.logging.LogLevel;
import io.netty.handler.logging.LoggingHandler;
import io.netty.handler.stream.ChunkedWriteHandler;
import lombok.extern.slf4j.Slf4j;
import org.example.common.utils.IpUtils;

/**
 * websocket 服务
 * @author cheval
 */
@Slf4j
public class WSServer {

    /** 用于保证服务只启动一次 */
    private static boolean flag = false;
    /** 专门处理连接事件的循环组 */
    private NioEventLoopGroup bossEventLoopGroup;
    /** 专门处理消息读写的事件循环组 */
    private NioEventLoopGroup workEventLoopGroup;
    private String endpoint = "/ws";
    private Integer port = 8765;

    public WSServer() {

    }

    public WSServer(Integer port) {
        this.port = port;
    }


    public WSServer(String endpoint,Integer port) {
        this.endpoint = endpoint;
        this.port = port;
    }


    public void start() {
        if (flag) {
            return;
        }
        ServerBootstrap bootstrap = null;
        try{
            bossEventLoopGroup = new NioEventLoopGroup();
            workEventLoopGroup = new NioEventLoopGroup();
            bootstrap = new ServerBootstrap()
                    .group(bossEventLoopGroup,workEventLoopGroup)
                    .channel(NioServerSocketChannel.class)
                    // 开启全局日志的处理器
                    .handler(new LoggingHandler(LogLevel.INFO))
                    .childHandler(new ChannelInitializer<NioSocketChannel>() {
                        @Override
                        public void initChannel(NioSocketChannel socketChannel) throws Exception {
                            ChannelPipeline pipeline = socketChannel.pipeline();
                            // 使用http消息编码解码器
                            pipeline.addLast(new HttpServerCodec());
                            // 以块的方式向channel写消息，添加ChunkedWriteHandler()
                            pipeline.addLast(new ChunkedWriteHandler());
                            // HttpObjectAggregator可以将http分段的数据报聚合起来
                            pipeline.addLast(new HttpObjectAggregator(8192));
                            // 对于websocket的数据是以帧的形式传递的,将http协议升级为ws协议并注册ws服务端点
                            // ws://localhost:8080/ws
                            pipeline.addLast(new WebSocketServerProtocolHandler(endpoint));
                            // 自定义handler,处理业务逻辑，以帧的形式处理
                            pipeline.addLast(new TextWebSocketFrameHandler());
                        }
                    });
            log.info("websocket服务启动成功,服务访问地址为 =======> {}","ws://" + IpUtils.getLocalIP() + ":" + port + endpoint);
            bootstrap.bind(port).sync().channel().closeFuture().sync();
        }catch (Exception e) {
            e.printStackTrace();
        } finally {
            bossEventLoopGroup.shutdownGracefully();
            workEventLoopGroup.shutdownGracefully();
        }
        flag = true;
    }
}
