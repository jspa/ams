package org.example.ams.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.system.SysUser;
import org.example.ams.log.Log;
import org.example.ams.service.system.ISysUserRoleService;
import org.example.ams.service.system.ISysUserService;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Locale;
import java.util.Map;

@Api(tags = "系统用户接口")
@RestController
@RequestMapping("/user")
public class SysUserController {

    @Autowired
    private ISysUserService sysUserService;
    @Autowired
    private ISysUserRoleService sysUserRoleService;

    @ApiOperation("获取所有用户信息")
    @PreAuthorize("hasAnyAuthority('system:user:list','*:*:*')")
    @GetMapping("/list")
    public CommonResult<Map<String,Object>> findUserByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                       @RequestParam(defaultValue = "10") Integer pageSize,
                                                       String username) {
        Map<String,Object> res = sysUserService.queryUserByPage(currentPage,pageSize,username);
        return CommonResult.success(res,null);
    }

    @Log(value = "添加用户",ifRecordParam = 1)
    @ApiOperation("添加用户")
    @PreAuthorize("hasAnyAuthority('system:user:add','*:*:*')")
    @PostMapping("/add")
    public CommonResult<Object> addUser(@RequestBody SysUser sysUser) {
        // 0: 成功 1,2失败
        Integer code = sysUserService.addUser(sysUser);
        if (code == 0) {
            return CommonResult.success(null);
        } else if(code == 1) {
            return CommonResult.failed("用户名不能设置为admin!");
        } else {
            return CommonResult.failed("业务繁忙，请稍后再试！");
        }
    }

    @Log(value = "修改用户",ifRecordParam = 1)
    @ApiOperation("修改用户")
    @PreAuthorize("hasAnyAuthority('system:user:update','*:*:*')")
    @PutMapping("/update")
    public CommonResult<Object> updateUser(@RequestBody SysUser user) {
        sysUserService.update(user);
        return CommonResult.success(null);
    }

    @Log(value = "删除用户",ifRecordParam = 1)
    @ApiOperation("删除用户")
    @PreAuthorize("hasAnyAuthority('system:user:delete','*:*:*')")
    @DeleteMapping("/delete")
    public CommonResult<Object> deleteUser(Integer[] ids) {
        sysUserService.deleteByIds(ids);
        return CommonResult.success(null);
    }

    @Log(value = "修改用户账号状态",ifRecordParam = 1)
    @ApiOperation("修改用户账号状态")
    @PreAuthorize("hasAnyAuthority('system:user:update','*:*:*')")
    @PutMapping("/modifyStatus")
    public CommonResult modifyUserStatus(@RequestParam(required = true) Integer userId) {
        sysUserService.updateStatus(userId);
        return CommonResult.success(null,"修改成功");
    }

    @Log(value = "删除用户角色",ifRecordParam = 1)
    @ApiOperation("删除指定的角色")
    @PreAuthorize("hasAnyAuthority('system:user:update','*:*:*')")
    @DeleteMapping("/deleteRole")
    public CommonResult deleteUserRole(@RequestParam(required = true) Integer userId,@RequestParam(required = true) Integer roleId) {
        sysUserRoleService.deleteByUserIdAndRoleId(userId,roleId);
        return CommonResult.success(null);
    }

    @Log(value = "添加用户角色",ifRecordParam = 1)
    @ApiOperation("给用户添加角色")
    @PreAuthorize("hasAnyAuthority('system:user:update','*:*:*')")
    @PostMapping("/addRole")
    public CommonResult add(@RequestParam(required = true) Integer userId, @RequestParam(required = true) Integer[] roleIds) {
        sysUserRoleService.add(userId,roleIds);
        return CommonResult.success(null,"添加成功");
    }
}
