package org.example.ams.controller.system.workflow;

import io.swagger.annotations.Api;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * 流程部署
 * @author cheval
 */
@Api(tags = "流程部署管理接口")
@RestController
@RequestMapping("/flow/manage")
public class FlowDeploymentController {

}
