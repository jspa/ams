package org.example.ams.controller.system;

import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.system.SysDictData;
import org.example.ams.service.system.ISysDictDataService;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

/**
 * @author cheval
 * 注意： 字典管理使用了缓存，添加新功能的时候注意更新缓存
 */
@RestController
@RequestMapping(value = "/dictData")
public class DictDataController {
    @Autowired
    private ISysDictDataService sysDictDataService;

    @ApiOperation("获取指定类型的字典数据")
    @GetMapping("/type/data")
    public CommonResult<Map<String,List<SysDictData>>> getDictDataByDictType(@RequestParam(required = true) String dictType) {
        if (dictType == null) {
            return null;
        }
        Map<String,List<SysDictData>> res = sysDictDataService.queryDictDataByDictType(dictType);
        return CommonResult.success(res,null);
    }

    @ApiOperation("更新字典数据")
    @PreAuthorize("hasAnyAuthority('system:dict:update','*:*:*')")
    @PutMapping("/update")
    public CommonResult modifyDictData(@RequestBody SysDictData sysDictData) {
        sysDictDataService.update(sysDictData);
        return CommonResult.success(null);
    }


    @ApiOperation("添加字典数据")
    @PreAuthorize("hasAnyAuthority('system:dict:add','*:*:*')")
    @PostMapping("/add")
    public CommonResult saveDictData(@RequestBody SysDictData sysDictData) {
        sysDictDataService.add(sysDictData);
        return CommonResult.success(null);
    }


    @ApiOperation("删除字典数据")
    @PreAuthorize("hasAnyAuthority('system:dict:delete','*:*:*')")
    @PostMapping("/delete")
    public CommonResult removeDictData(@RequestBody SysDictData sysDictData) {
        sysDictDataService.delete(sysDictData);
        return CommonResult.success(null);
    }
}
