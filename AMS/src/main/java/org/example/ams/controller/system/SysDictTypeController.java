package org.example.ams.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.system.SysDictType;
import org.example.ams.service.system.ISysDictTypeService;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Map;

@Api(tags = "SysDictType接口")
@RestController
@RequestMapping("/sysDictType")
public class SysDictTypeController {

    @Autowired
    private ISysDictTypeService sysDictTypeService;

    @ApiOperation("分页查询列表")
    @PreAuthorize("hasAnyAuthority('system:sysdicttype:list','*:*:*')")
    @GetMapping("/list")
    public CommonResult<Map<String,Object>> listByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                                   @RequestParam(defaultValue = "10") Integer pageSize,
                                                                   HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Map<String,Object> res = sysDictTypeService.queryByPage(currentPage,pageSize,params);
        return CommonResult.success(res,null);
    }

    @ApiOperation("添加")
    @PreAuthorize("hasAnyAuthority('system:sysdicttype:add','*:*:*')")
    @PostMapping("/add")
    public CommonResult add(@RequestBody SysDictType sysDictType){
            sysDictTypeService.save(sysDictType);
        return CommonResult.success(null,"添加成功");
    }


    @ApiOperation("检查字典类型是否已近存在")
    @PreAuthorize("hasAnyAuthority('system:sysdicttype:add','*:*:*')")
    @GetMapping("/check")
    public CommonResult checkTypeExist(String dictType){
            SysDictType sysDictType = sysDictTypeService.queryByType(dictType);
            if (sysDictType != null) {
                return CommonResult.success(true,null);
            }
        return CommonResult.success(false,null);
    }

    @ApiOperation("修改")
    @PreAuthorize("hasAnyAuthority('system:sysdicttype:update','*:*:*')")
    @PutMapping("/modify")
    public CommonResult modify(@RequestBody SysDictType sysDictType) {
            sysDictTypeService.update(sysDictType);
        return CommonResult.success(null,"修改成功");
    }


    @ApiOperation("删除")
    @PreAuthorize("hasAnyAuthority('system:sysdicttype:delete','*:*:*')")
    @DeleteMapping("/delete")
    public CommonResult delete(Integer[] ids){
        sysDictTypeService.delete(ids);
        return CommonResult.success(null,"删除成功");
    }
}