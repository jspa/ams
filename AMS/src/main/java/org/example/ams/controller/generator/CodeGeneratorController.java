package org.example.ams.controller.generator;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.generator.TableFields;
import org.example.ams.service.generator.ICodeGeneratorService;
import org.example.ams.util.GenUtils;
import org.example.ams.vo.CodeGenVo;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import java.io.File;
import java.io.FileOutputStream;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;

@Api(tags = "代码生成器接口")
@RequestMapping("/gen")
@RestController
public class CodeGeneratorController {

    @Autowired
    private ICodeGeneratorService codeGeneratorService;

    @ApiOperation("代码生成")
    @PreAuthorize("hasAnyAuthority('system:gen:generator','*:*:*')")
    @PostMapping("/generate")
    public CommonResult generate(@RequestBody CodeGenVo codeGenVo) {
        // 判断是否要生成前端代码
        Boolean isVueCode = codeGenVo.getTemplateParams().getIsVueCode();

        try {
            String outPath = codeGenVo.getTemplateParams().getOutPath() + File.separator + codeGenVo.getTemplateParams().getModule() + ".zip";
            FileOutputStream outputStream = new FileOutputStream(new File(outPath));

            ZipOutputStream zipOutputStream = new ZipOutputStream(outputStream);
            Map<String, Object> templateParams = new HashMap<>();
            templateParams.put("className", codeGenVo.getTemplateParams().getClassName());
            templateParams.put("package", codeGenVo.getTemplateParams().getPackageName());
            templateParams.put("module", codeGenVo.getTemplateParams().getModule());
            templateParams.put("tableFields", codeGenVo.getTableFields());
            templateParams.put("isVueCode", codeGenVo.getTemplateParams().getIsVueCode());
            GenUtils.generatorCode(templateParams, GenUtils.getTemplates(isVueCode), zipOutputStream);
            zipOutputStream.close();
        } catch (Exception e) {
            e.printStackTrace();
            return CommonResult.failed("代码生成异常,请检查模板参数是否正确！");
        }


        return CommonResult.success(null, "代码生成成功");
    }

    @ApiOperation("获取系统所用表名")
    @PreAuthorize("hasAnyAuthority('system:gen:generator','*:*:*')")
    @GetMapping("/tables")
    public CommonResult<List<String>> findAllTable() {
        List<String> tables = codeGeneratorService.queryAllTables();
        return CommonResult.success(tables, null);
    }


    @ApiOperation("获取表的所有字段信息")
    @PreAuthorize("hasAnyAuthority('system:gen:generator','*:*:*')")
    @GetMapping("/fields")
    public CommonResult<List<TableFields>> findTableFields(@RequestParam(required = true) String tableName) {
        List<TableFields> tableFields = codeGeneratorService.queryTableFields(tableName);
        return CommonResult.success(tableFields, null);
    }
}
