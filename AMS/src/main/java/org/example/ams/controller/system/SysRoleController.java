package org.example.ams.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.system.SysRole;
import org.example.ams.entity.system.SysRoleMenu;
import org.example.ams.service.system.ISysRoleMenuService;
import org.example.ams.service.system.ISysRoleService;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;

@Api(tags = "系统角色接口")
@RestController
@RequestMapping("/role")
public class SysRoleController {
    @Autowired
    private ISysRoleService sysRoleService;
    @Autowired
    private ISysRoleMenuService roleMenuService;

    @ApiOperation("查询指定用户没有的角色")
    @PreAuthorize("hasAnyAuthority('system:user:update','*:*:*')")
    @GetMapping("/userNoHav")
    public CommonResult<List<SysRole>> listNoHavRoleForUser(@RequestParam(required = true) Integer userId) {
        List<SysRole> sysRoles = sysRoleService.queryNoHavRoleForUser(userId);
        return CommonResult.success(sysRoles,null);
    }

    @ApiOperation("分页查询列表")
    @PreAuthorize("hasAnyAuthority('system:sysrole:list','*:*:*')")
    @GetMapping("/list")
    public CommonResult<Map<String,Object>> listByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                       @RequestParam(defaultValue = "10") Integer pageSize,
                                                       HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Map<String,Object> res = sysRoleService.queryByPage(currentPage,pageSize,params);
        return CommonResult.success(res,null);
    }

    @ApiOperation("添加")
    @PreAuthorize("hasAnyAuthority('system:sysrole:add','*:*:*')")
    @PostMapping("/add")
    public CommonResult add(@RequestBody SysRole sysRole){
        sysRoleService.save(sysRole);
        return CommonResult.success(null,"添加成功");
    }

    @ApiOperation("修改")
    @PreAuthorize("hasAnyAuthority('system:sysrole:update','*:*:*')")
    @PutMapping("/modify")
    public CommonResult modify(@RequestBody SysRole sysRole) {
        sysRoleService.update(sysRole);
        return CommonResult.success(null,"修改成功");
    }


    @ApiOperation("删除")
    @PreAuthorize("hasAnyAuthority('system:sysrole:delete','*:*:*')")
    @DeleteMapping("/delete")
    public CommonResult delete(@RequestParam(required = true) Integer[] ids){
        sysRoleService.delete(ids);
        return CommonResult.success(null,"删除成功");
    }

    @ApiOperation("改变状态")
    @PreAuthorize("hasAnyAuthority('system:sysrole:update','*:*:*')")
    @PutMapping("/changeStatus")
    public CommonResult changeStatus(@RequestParam(required = true) Integer id) {
        boolean flag = sysRoleService.updateStatus(id);
        if (flag) {
            return CommonResult.success(null);
        }
        return CommonResult.failed();
    }

    @ApiOperation("获取角色已经拥有的权限")
    @PreAuthorize("hasAnyAuthority('system:sysrole:dispatchauth','*:*:*')")
    @GetMapping("/havAuths")
    public CommonResult<List<Integer>> getRoleHavMenuIds(@RequestParam(required = true) Integer roleId) {
        List<SysRoleMenu> auths =  roleMenuService.queryByRoleId(roleId);
        List<Integer> res = null;
        if (auths != null && auths.size() > 0) {
            res = auths.stream().map(item -> {
                return item.getMenuId();
            }).collect(Collectors.toList());
        }
        return CommonResult.success(res,null);
    }

    @ApiOperation("给角色分配权限")
    @PreAuthorize("hasAnyAuthority('system:sysrole:dispatchauth','*:*:*')")
    @PostMapping("/dispatchAuth")
    public CommonResult dispatchAuth(@RequestParam(required = true) Integer roleId,@RequestParam(required = true) Integer[] menuIds) {
        boolean flag = roleMenuService.updateAuth(roleId,menuIds);
        if (flag) {
            return CommonResult.success(null);
        }
        return CommonResult.failed();
    }
}
