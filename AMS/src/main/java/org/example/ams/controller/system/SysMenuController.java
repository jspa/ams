package org.example.ams.controller.system;

import io.swagger.annotations.ApiOperation;
import org.example.ams.entity.system.SysMenu;
import org.example.ams.service.system.ISysMenuService;
import org.example.common.api.CommonResult;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.List;
import java.util.Map;


@RequestMapping("/menu")
@RestController
public class SysMenuController {

    @Autowired
    private ISysMenuService sysMenuService;

    /**
     * 获取导航菜单
     * 条件：
     * 1）当前登录用户拥有的菜单
     * 2）菜单类型为 1/2（目录和菜单）
     * 3）未被删除 is_validate = 1
     * 4）未被禁用 enable = 1
     * 5）树形结构
     */
    @GetMapping("/tree")
    public CommonResult<List<SysMenu>> getMenuTree() {
        List<SysMenu> sysMenuTree = sysMenuService.queryMenuTree();
        return CommonResult.success(sysMenuTree,null);
    }


    /**
     * 获取所有权限
     * 条件：
     * 1) 所有菜单
     * 2）未被删除 is_validate = 1
     * 3）树形结构
     */
    @GetMapping("/auth/tree")
    public CommonResult<List<SysMenu>> getTree() {
        List<SysMenu> sysMenuTree = sysMenuService.queryAuthTree();
        return CommonResult.success(sysMenuTree,null);
    }


    @ApiOperation("分页查询列表")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:list','*:*:*')")
    @GetMapping("/list")
    public CommonResult<Map<String,Object>> listByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                       @RequestParam(defaultValue = "10") Integer pageSize,
                                                       HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Map<String,Object> res = sysMenuService.queryByPage(currentPage,pageSize,params);
        return CommonResult.success(res,null);
    }

    @ApiOperation("添加")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:add','*:*:*')")
    @PostMapping("/add")
    public CommonResult add(@RequestBody SysMenu sysMenu){
        sysMenuService.save(sysMenu);
        return CommonResult.success(null,"添加成功");
    }

    @ApiOperation("修改")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:update','*:*:*')")
    @PutMapping("/modify")
    public CommonResult modify(@RequestBody SysMenu sysMenu) {
        sysMenuService.update(sysMenu);
        return CommonResult.success(null,"修改成功");
    }


    @ApiOperation("删除")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:delete','*:*:*')")
    @DeleteMapping("/delete")
    public CommonResult delete(Integer[] ids){
        sysMenuService.delete(ids);
        return CommonResult.success(null,"删除成功");
    }


    @ApiOperation("获取所有目录")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:list','*:*:*')")
    @GetMapping("/list/directory")
    public CommonResult<List<SysMenu>> findAllDirectory() {
        List<SysMenu> directories = sysMenuService.queryAllDirectory();
        return CommonResult.success(directories,null);
    }

    @ApiOperation("检查ID是否已经存在")
    @PreAuthorize("hasAnyAuthority('system:sysmenu:add','*:*:*')")
    @GetMapping("/check/id")
    public CommonResult<Boolean> checkIDExist(@RequestParam(required = true) Integer menuId) {
        boolean flag = sysMenuService.checkIDExist(menuId);
        return CommonResult.success(flag,null);
    }
}
