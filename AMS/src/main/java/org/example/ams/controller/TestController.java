package org.example.ams.controller;


import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;


@RestController
public class TestController {

    @PreAuthorize("hasAnyAuthority('system:user:test','*:*:*')")
    @GetMapping("/test")
    public String test() {


        return "hello world";
    }


    public static void main(String[] args) {
        String a = "3";
        String[] arr = a.split(".");
    }
}
