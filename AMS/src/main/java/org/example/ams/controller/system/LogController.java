package org.example.ams.controller.system;

import io.swagger.annotations.Api;
import io.swagger.annotations.ApiOperation;
import org.example.ams.service.system.ISysLogService;
import org.example.common.api.CommonResult;
import org.example.common.utils.IpUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.*;

import javax.servlet.http.HttpServletRequest;
import java.util.Date;
import java.util.Map;

@Api(tags = "日志接口")
@RequestMapping("/log")
@RestController
public class LogController {

    @Autowired
    private ISysLogService sysLogService;

    @PreAuthorize("hasAnyAuthority('*:*:*')")
    @ApiOperation("获取ip属地")
    @GetMapping("/ipinfo")
    public CommonResult<String> getIpInfo(@RequestParam(required = true) String ip) {
        String address = IpUtils.getAddress(ip);
        return CommonResult.success(address,null);
    }


//    @PreAuthorize("hasAnyAuthority('*:*:*')")
//    @ApiOperation("查询所有日志")
//    @GetMapping("/list")
//    public CommonResult<Map<String,Object>> list(@RequestParam(defaultValue = "1") Integer currentPage,
//                                           @RequestParam(defaultValue = "20") Integer pageSize,
//                                           String user,
//                                           Date[] optionDate) {
//        CommonResult<Map<String,Object>> res = sysLogService.queryByPage(currentPage,pageSize,user,optionDate);
//        return res;
//    }

    @ApiOperation("分页查询列表")
    @PreAuthorize("hasAnyAuthority('system:syslog:list','*:*:*')")
    @GetMapping("/list")
    public CommonResult<Map<String,Object>> listByPage(@RequestParam(defaultValue = "1") Integer currentPage,
                                                       @RequestParam(defaultValue = "10") Integer pageSize,
                                                       HttpServletRequest request) {
        Map<String, String[]> params = request.getParameterMap();
        Map<String,Object> res = sysLogService.queryByPage(currentPage,pageSize,params);
        return CommonResult.success(res,null);
    }

    @ApiOperation("删除")
    @PreAuthorize("hasAnyAuthority('system:syslog:delete','*:*:*')")
    @DeleteMapping("/delete")
    public CommonResult delete(Integer[] ids){
        sysLogService.delete(ids);
        return CommonResult.success(null,"删除成功");
    }

    @ApiOperation("获取ip归属地")
    @PreAuthorize("hasAnyAuthority('system:syslog:list','*:*:*')")
    @GetMapping("/ipaddress")
    public CommonResult<String> getIpAddress(String ip) {
        return CommonResult.success(IpUtils.getAddress(ip),null);
    }
}
