package org.example.ams.vo;

import lombok.Data;

/**
 * 用户一个月的签到数据
 * @author cheval
 */
@Data
public class SignDataVo {

    /** 本月天数 */
    private Integer days;
    /** 签到数据 */
    private String signData;
    /** 连续签到天数 */
    private Integer count;
    /** 签到天数 */
    private Integer signCount;
}
