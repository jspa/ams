package org.example.ams.vo;

import lombok.Data;

@Data
public class LoginUser {
    private String username;
    private String password;
    private String code; // 验证码
}
