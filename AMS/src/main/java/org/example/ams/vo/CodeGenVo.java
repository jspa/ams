package org.example.ams.vo;

import lombok.Data;
import org.example.ams.entity.generator.TableFields;
import org.example.ams.entity.generator.TemplateParam;

import java.io.Serializable;
import java.util.List;

/**
 * @author cheval
 */
@Data
public class CodeGenVo implements Serializable {
    private static final long serialVersionUID = 42L;
    private TemplateParam templateParams;
    private List<TableFields> tableFields;
}
