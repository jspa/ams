admin：
 username 不可以修改


redisson:
 Rlock 常用的方法
    void lock();
    void lock(long leaseTime, TimeUnit unit);
    boolean tryLock(long time, TimeUnit unit) throws InterruptedException;
    boolean tryLock(long waitTime, long leaseTime, TimeUnit unit) throws InterruptedException;

    第一个方法void lock()：第一表示lock表示去加锁，加锁成功，没有返回值，继续执行下面代码；但是如果redis已经有这
    个锁了，它会一直阻塞，直到锁的时间失效（默认30秒），再继续往下执行。这个方法是要保证一定要抢到锁的，它的默认过期
    时间也是30秒，和tryLock()不同的是，它如果没抢占到锁，会一直自旋。
    第二个方法void lock(long leaseTime, TimeUnit unit)：和第一无参数lock逻辑一样，只是可以直接设置锁失效时间
    。用法：helloLock.lock(5, TimeUnit.SECONDS);。
    第三个方法两个参数的boolean tryLock(long time, TimeUnit unit)表示尝试去加锁（第一个参数表示the maximum
     time to wait for the lock），加锁成功,返回true，继续执行true下面代码；但是如果redis已经有这个锁了他会等待，
     还拿不到锁它会返回false，执行false的代码块。为了实现waitTime，使用了redis的订阅发布功能。也就是没有抢到锁的线
     程订阅消息，直至waitTime过期返回false或者被通知新一轮的开始抢占锁。当然，它如果抢占到锁，锁的过期时间也是30秒，
     同样也会存在一个定时任务续过期时间，保证业务执行时间不会超过过期时间，抢占失败即返回false。